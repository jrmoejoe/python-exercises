# Let’s say I give you a list saved in a variable: a = [1, 4, 9, 16, 25, 36, 49, 64, 81, 100].
# Write one line of Python that takes this list a and makes a new list that has only the even elements of this list in it.
# Modified: Take a random list of variable length

import random
a = []
for i in range(10):
    a.append(random.randint(1,100))

print(a)

newList = [element for element in a if element % 2 ==0]
print(newList)


