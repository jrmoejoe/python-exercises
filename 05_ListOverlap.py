# Take two lists, say for example these two:
#
#   a = [1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]
#   b = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]
# write a program that returns a list that contains only the elements that are common between the lists
# (without duplicates). Make sure your program works on two lists of different sizes.
#
# Extras:
# Randomly generate two lists to test this
import random

a = []
b = []

for x in range(random.randint(1,100)):
    a.append(random.randrange(1,101,1))

for y in range(random.randint(1,100)):
    b.append(random.randrange(1, 101, 1))

print("List a: " + str(a))
print("List b: " + str(b))

newList= []

for i in a:
    for j in b:
        if i==j and i not in newList:
            newList.append(i)


print("Common elements: " + str(newList))
