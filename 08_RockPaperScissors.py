# Make a two-player Rock-Paper-Scissors game. (Hint: Ask for player plays (using input), compare them,
# print out a message of congratulations to the winner, and ask if the players want to start a new game)
# Remember the rules:
# Rock beats scissors
# Scissors beats paper
# Paper beats rock
while True:
    player_a = input("Choose Rock, Paper or scissors: ").lower()

    player_b = input("Choose Rock, Paper or scissors: ").lower()

    if player_a == player_b:
        print("It's a tie!")
    elif player_a == 'rock':
        if player_b == 'scissors':
            print("Rock wins!")
        else:
            print("Paper wins!")
    elif player_a == 'scissors':
        if player_b == 'paper':
            print("Scissors win!")
        else:
            print("Rock wins!")
    elif player_a == 'paper':
        if player_b == 'rock':
            print("Paper wins!")
        else:
            print("Scissors win!")
    else:
        print("Invalid input! You have not entered rock, paper or scissors, try again.")

    again = input("type Again for another round or Quit to exit game: ").lower()

    if again == "again":
        continue
    else:
        print("Bye Bye...")
        break
