#Ask the user for a number and determine whether the number is prime or not. (For those who have forgotten,
# a prime number is a number that has no divisors.). You can (and should!) use your answer to Exercise 4 to help you.

def prime():
    x = int(input('Your number:\n'))
    for i in range(2, round(x/2)+2):
        if x % i == 0:
            print('Your number is a composite number')
            break
        elif i == round(x/2)+1:
            print('Your number is a prime number')
            break
prime()