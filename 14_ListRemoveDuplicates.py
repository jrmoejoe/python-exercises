# Write a program (function!) that takes a list and returns a new list that contains all the elements
# of the first list minus all the duplicates.
#
# Extras:
#
# Write two different functions to do this - one using a loop and constructing a list, and another using sets.
# Go back and do Exercise 5 using sets, and write the solution for that in a different function.

# definir funcion

    #create new empty list
    # loop through prev list and
        #if not in newlist, append to the new one

#create list
# pass it to the function
# print

def removeDuplicates(list):
    newList = []
    for i in list:
        if i not in newList:
            newList.append(i)
    return newList

list = [1,2,2,5,6,7,7,9]

result = removeDuplicates(list)
print(result)
