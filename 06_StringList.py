# Ask the user for a string and print out whether this string is a palindrome or not. (A palindrome is a string that
# reads the same forwards and backwards.)

string = input("Enter a Word: ")

reverse = string[::-1]

if string == reverse:
    print("Is A Palindrome")
else:
    print("Not A Palindrome")

# 2nd solution

def reverse(word):
    x = ''
    for i in range(len(word) - 1,-1,-1):
        x += word[i]
    return x

word = input('give me a word:\n')
x = reverse(word)
print(f"Reversed word: {x}")
if x == word:
    print('This is a Palindrome')
else:
    print('This is NOT a Palindrome')




