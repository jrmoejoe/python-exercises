# Generate a random number between 1 and 9 (including 1 and 9). Ask the user to guess the number, then tell them
# whether they guessed too low, too high, or exactly right. (Hint: remember to use the user input lessons from the
# very first exercise)
# Extras:
# Keep the game going until the user types “exit”
# Keep track of how many guesses the user has taken, and when the game ends, print this out.
import random
count = 1
exit = ""
while exit != "exit":
    number = random.randint(1, 9)
    guess = int(input("Guess a number between 1 and 9: "))
    if guess < number:
        print("Too low")
        count += 1
    elif guess > number:
        print("Too High")
        count += 1
    else:
        print(f"Correct! it took you {count} times")
        exit = input("Type exit to quit game or continue to keep playing: ")
print("Bye Bye..")
